from codima.pascal import binom
from codima.timer import Timer


def main(debug: bool = False):
    with Timer("1st 23 lines of pascal's triangle with memorization"):
        for n in range(23):
            for k in range(n):
                out: int = binom(n, k)
                if debug: print(out, end = ' ')
            if debug: print('')
    with Timer("1st 23 lines of pascal's triangle without memorization"):
        for n in range(23):
            for k in range(n):
                out: int = binom(n, k, memorize = False)
                if debug: print(out, end = ' ')
            if debug: print('')


if __name__ == '__main__': main()
