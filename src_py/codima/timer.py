import time

from typing import Optional, Callable


def propriety_os_copy_simulation():
    for i in range(100):
        for j in range(10000000): pass
        bar = '='*(i//10)
        print(f'{bar}[{i}%] progress')
    time.sleep(3)


class Timer(object):
    name: str = None
    time_start: float = None
    time_time: float = None
    perf_start: float = None
    perf_time: float = None
    logger_func = None

    def __init__(self, name: str): self.name = name

    def __enter__(self, logger_func: Optional[Callable[[str], None]] = None) -> 'Timer': return self.start(logger_func)

    def start(self, logger_func: Optional[Callable[[str], None]] = None) -> 'Timer':
        if logger_func is None: logger_func = print
        self.perf_start = time.perf_counter_ns()
        self.time_start = time.time_ns()
        self.logger_func = logger_func
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None: self.stop()

    def stop(self) -> None:
        self.perf_time = time.perf_counter_ns() - self.perf_start
        self.time_time = time.time_ns() - self.time_start
        self.logger_func(str(self))

    def __str__(self, print_sec: bool = True, print_time: bool = False) -> str:
        out: str = f'Timer: {self.name}'
        fac: float = 1.0
        unit: str = '(ns)'
        if print_sec: fac, unit = 1.0e-9, '(s)'
        if print_time:
            if not (self.time_time is None): out += f' | took (time): {self.time_time*fac} {unit}'
            elif not (self.time_start is None): out += f' | started (time): {self.time_start*fac} {unit}'
        else:
            if not (self.perf_time is None): out += f' | took (perf): {self.perf_time*fac} {unit}'
            elif not (self.time_start is None): out += f' | started (perf): {self.perf_start*fac} {unit}'
        return out
